%function loading data from websites
function [station1MeteoData_wttrin, station1MeteoData_imgw, station2MeteoData_wttrin, station2MeteoData_imgw] = loadData(station1URL_wttrin, station1URL_imgw, station2URL_wttrin, station2URL_imgw)    

station1URL_wttrin = 'http://wttr.in/katowice?format=j1';
station1MeteoData_wttrin = webread(station1URL_wttrin) 
station1URL_imgw = 'https://danepubliczne.imgw.pl/api/data/synop/id/12560/format/json';
station1MeteoData_imgw = webread(station1URL_imgw)
station2URL_wttrin = 'http://wttr.in/raciborz?format=j1';
station2MeteoData_wttrin = webread(station2URL_wttrin)
station2URL_imgw = 'https://danepubliczne.imgw.pl/api/data/synop/id/12540/format/json';
station2MeteoData_imgw = webread(station2URL_imgw)

end
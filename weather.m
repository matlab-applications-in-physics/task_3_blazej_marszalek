%MATLAB 2020b
%name: weather
%author: blazej_marszalek
%date: 28.11.2020
%version: 1.0

%loading this data every hour
t = timer;
t.Period = 3600;
t.TasksToExecute = inf;
t.ExecutionMode = 'fixedRate';
t.TimerFcn = @(src, event) run('loadData');
start(t)

%putting measured values (temperature, pressure, speed of wind, humidity)
%with time of measuremets in a matrix

time = clock
year = time(1)
month = time(2)
day = time(3)
hour = time(4)
minutes = time(5)

M_Station1_wttrin = [year month day hour minutes str2num(station1MeteoData_wttrin.current_condition.temp_C) str2num(station1MeteoData_wttrin.current_condition.pressure) str2num(station1MeteoData_wttrin.current_condition.windspeedKmph) str2num(station1MeteoData_wttrin.current_condition.humidity)]
M_Station1_imgw = [year month day hour minutes str2num(station1MeteoData_imgw.temperatura) str2num(station1MeteoData_imgw.cisnienie) str2num(station1MeteoData_imgw.predkosc_wiatru) str2num(station1MeteoData_imgw.wilgotnosc_wzgledna)]
    
M_Station2_wttrin = [year month day hour minutes str2num(station2MeteoData_wttrin.current_condition.temp_C) str2num(station2MeteoData_wttrin.current_condition.pressure) str2num(station2MeteoData_wttrin.current_condition.windspeedKmph) str2num(station2MeteoData_wttrin.current_condition.humidity)]
M_Station2_imgw = [year month day hour minutes str2num(station2MeteoData_imgw.temperatura) str2num(station2MeteoData_imgw.cisnienie) str2num(station2MeteoData_imgw.predkosc_wiatru) str2num(station2MeteoData_imgw.wilgotnosc_wzgledna )]

%adding headers to each columne in every matrix
header = {'Year','Month', 'Day', 'Hour', 'Minutes', 'Temperature [oC]', 'Pressure [hPa]', 'Speed of Wind [km/h]', 'Humidity [%]'};

output1 = [header; num2cell(M_Station1_wttrin)]
output2 = [header; num2cell(M_Station1_imgw)]
output3 = [header; num2cell(M_Station2_wttrin)]
output4 = [header; num2cell(M_Station1_imgw)]

%setting time of sunrise and sunset
sunrise  = station1MeteoData_wttrin.weather(1).astronomy.sunrise  
hour_Sunrise = str2num(sunrise(1:2))
minutes_Sunrise = str2num(sunrise(4:5))

sunset  = station1MeteoData_wttrin.weather(1).astronomy.sunset  
hour_Sunrise = str2num(sunset(1:2))
minutes_Sunrise = str2num(sunset(4:5))

%converting way of counting time to the one used by system
if sunrise(7:8) == 'PM'
    hour_Sunrise = str2num(sunrise(1:2)) + 12
end

if sunset(7:8) == 'PM'
    hour_Sunset = str2num(sunset(1:2)) + 12
end

%convertin given time in minutes
time_Sunrise = hour_Sunrise*60 + minutes_Sunrise
time_Sunset = hour_Sunset*60 + minutes_Sunset
time_current = hour*60 + minutes

%putting loaded date in csv files with division in day and night
if time_current >= time_Sunrise && time_current < time_Sunset
    dlmwrite('data_Station1_wttrin_Day.csv',M_Station1_wttrin,'delimiter',',','-append');
    dlmwrite('data_Station1_imgw_Day.csv',M_Station1_imgw,'delimiter',',','-append');
    dlmwrite('data_Station2_wttrin_Day.csv',M_Station2_wttrin,'delimiter',',','-append');
    dlmwrite('data_Station2_imgw_Day.csv',M_Station2_imgw,'delimiter',',','-append');
else
    dlmwrite('data_Station1_wttrin_Night.csv',M_Station1_wttrin,'delimiter',',','-append');
    dlmwrite('data_Station1_imgw_Night.csv',M_Station1_imgw,'delimiter',',','-append');
    dlmwrite('data_Station2_wttrin_Night.csv',M_Station2_wttrin,'delimiter',',','-append');
    dlmwrite('data_Station2_imgw_Night.csv',M_Station2_imgw,'delimiter',',','-append');
end

%calculate sensed temperature
T1 = 13.12 + 0.6215*str2num(station1MeteoData_imgw.temperatura) - 11.37* str2num(station1MeteoData_imgw.predkosc_wiatru)^(0.16) + 0.3965* str2num(station1MeteoData_imgw.temperatura)* str2num(station1MeteoData_imgw.predkosc_wiatru)^(0.16)
T2 = 13.12 + 0.6215*str2num(station2MeteoData_imgw.temperatura) - 11.37* str2num(station2MeteoData_imgw.predkosc_wiatru)^(0.16) + 0.3965* str2num(station2MeteoData_imgw.temperatura)* str2num(station2MeteoData_imgw.predkosc_wiatru)^(0.16)

%puttin them with time into a csv file with division to day and night
T = [year month day hour minutes T1 T2]

header = {'Year','Month', 'Day', 'Hour', 'Minutes', 'Sensed Temperature in Katowice [oC]', 'Sensed Temperature in Raciborz [oC]'}
output5 = [header; num2cell(T)]

if time_current >= time_Sunrise && time_current < time_Sunset
    dlmwrite('sensed_temperature_Day.csv', T,'delimiter',',','-append');
else
    dlmwrite('sensed_temperature_Night.csv', T,'delimiter',',','-append');
end

%putting calculated values into a table
T_temperature_sensed_Day = readtable('sensed_temperature_Day.csv')
T_temperature_sensed_Night = readtable('sensed_temperature_Night.csv')

%calculate average sensed temperature day and night
sum_T_temperature_sensed_Katowice_Day = varfun(@sum,T_temperature_sensed_Day(1:height(T_temperature_sensed_Day),6))
sum_T_temperature_sensed_Katowice_Night = varfun(@sum,T_temperature_sensed_Night(1:height(T_temperature_sensed_Night),6))

sum_T_temperature_sensed_Raciborz_Day = varfun(@sum,T_temperature_sensed_Day(1:height(T_temperature_sensed_Day),7))
sum_T_temperature_sensed_Raciborz_Night = varfun(@sum,T_temperature_sensed_Night(1:height(T_temperature_sensed_Night),7))

sum_T_temperature_sensed_Katowice_Day = sum_T_temperature_sensed_Day{1, 1}
sum_T_temperature_sensed_Katowice_Night = sum_T_temperature_sensed_Night{1, 1}

sum_T_temperature_sensed_Raciborz_Day = sum_T_temperature_sensed_Day{1, 2}
sum_T_temperature_sensed_Raciborz_Night = sum_T_temperature_sensed_Night{1, 2}

average_T_sensed_Katowice_Day = sum_T_temperature_sensed_Katowice_Day/height(T_temperature_sensed_Day)
average_T_sensed_Katowice_Night = sum_T_temperature_sensed_Katowice_Night/height(T_temperature_sensed_Night)

average_T_sensed_Raciborz_Day = sum_T_temperature_sensed_Raciborz_Day/height(T_temperature_sensed_Day)
average_T_sensed_Raciborz_Night = sum_T_temperature_sensed_Raciborz_Night/height(T_temperature_sensed_Night)

%putting meteo data from website to a table
T_Katowice_wttrin_Day = readtable('data_Station1_wttrin_Day.csv')
T_Katowice_wttrin_Night = readtable('data_Station1_wttrin_Night.csv')
T_Katowice_imgw_Day = readtable('data_Station1_imgw_Day.csv')
T_Katowice_imgw_Night = readtable('data_Station1_imgw_Night.csv')

T_Raciborz_wttrin_Day = readtable('data_Station2__wttrin_Day.csv')
T_Raciborz_wttrin_Night = readtable('data_Station2__wttrin_Night.csv')
T_Raciborz_imgw_Day = readtable('data_Station2__imgw_Day.csv')
T_Raciborz_imgw_Night = readtable('data_Station2__imgw_Night.csv')

%calculate average temperature
sum_T_Katowice_wttrin_Day = varfun(@sum,T_Katowice_wttrin_Day(1:height(T_Katowice_wttrin_Day),6))
sum_T_Katowice_wttrin_Night = varfun(@sum,T_Katowice_wttrin_Night(1:height(T_Katowice_wttrin_Night),6))
sum_T_Katowice_imgw_Day = varfun(@sum,T_Katowice_imgw_Day(1:height(T_Katowice_imgw_Day),6))
sum_T_Katowice_imgw_Night = varfun(@sum,T_Katowice_imgw_Night(1:height(T_Katowice_imgw_Night),6))

sum_T_Raciborz_wttrin_Day = varfun(@sum,T_Raciborz_wttrin_Day(1:height(T_Raciborz_wttrin_Day),6))
sum_T_Raciborz_wttrin_Night = varfun(@sum,T_Raciborz_wttrin_Night(1:height(T_Raciborz_wttrin_Night),6))
sum_T_Raciborz_imgw_Day = varfun(@sum,T_Raciborz_imgw_Day(1:height(T_Raciborz_imgw_Day),6))
sum_T_Raciborz_imgw_Night = varfun(@sum,T_Raciborz_imgw_Night(1:height(T_Raciborz_imgw_Night),6))

sum_T_Katowice_wttrin_Day = sum_T_Katowice_wttrin_Day{1, 1}
sum_T_Katowice_wttrin_Night = sum_T_Katowice_wttrin_Night{1, 1}
sum_T_Katowice_imgw_Day = sum_T_Katowice_imgw_Day{1, 1}
sum_T_Katowice_imgw_Night = sum_T_Katowice_imgw_Night{1, 1}

sum_T_Raciborz_wttrin_Day = sum_T_Raciborz_wttrin_Day{1, 1}
sum_T_Raciborz_wttrin_Night = sum_T_Raciborz_wttrin_Night{1, 1}
sum_T_Raciborz_imgw_Day = sum_T_Raciborz_imgw_Day{1, 1}
sum_T_Raciborz_imgw_Night = sum_T_Raciborz_imgw_Night{1, 1}


average_T_Katowice_wttrin_Day = sum_T_Katowice_wttrin_Day/height(T_Katowice_wttrin_Day)
average_T_Katowice_wttrin_Night = sum_T_Katowice_wttrin_Night/height(T_Katowice_wttrin_Night)
average_T_Katowice_imgw_Day = sum_T_Katowice_imgw_Day/height(T_Katowice_imgw_Day)
average_T_Katowice_imgw_Night = sum_T_Katowice_imgw_Night/height(T_Katowice_imgw_Night)

average_T_Raciborz_wttrin_Day = sum_T_Raciborz_wttrin_Day/height(T_Raciborz_wttrin_Day)
average_T_Raciborz_wttrin_Night = sum_T_Raciborz_wttrin_Night/height(T_Raciborz_wttrin_Night)
average_T_Raciborz_imgw_Day = sum_T_Raciborz_imgw_Day/height(T_Raciborz_imgw_Day)
average_T_Raciborz_imgw_Night = sum_T_Raciborz_imgw_Night/height(T_Raciborz_imgw_Night)

%puttin calculated average values into a csv file
M_average_T = [year month day hour minutes average_T_Katowice_wttrin_Day average_T_Katowice_wttrin_Night average_T_Katowice_imgw_Day average_T_Katowice_imgw_Night average_T_Raciborz_wttrin_Day average_T_Raciborz_wttrin_Night average_T_Raciborz_imgw_Day average_T_sensed_Katowice_Day average_T_sensed_Katowice_Night average_T_sensed_Raciborz_Day average_T_sensed_Raciborz_Night diff_average_Katowice_wttrin diff_average_Katowice_imgw diff_average_Raciborz_wttrin diff_average_Raciborz_imgw diff_average_sensed_Katowice diff_average_sensed_Raciborz]

header = {'Year','Month', 'Day', 'Hour', 'Minutes', 'Average Temperature Katowice Day (wttrin) [oC]', 'Average Temperature Katowice Night (wttrin) [oC]', 'Average Temperature Katowice Day (imgw) [oC]''Average Temperature Katowice Night (imgw) [oC]', 'Average Temperature Raciborz Day (wttrin) [oC]', 'Average Temperature Raciborz Night (wttrin) [oC]', 'Average Temperature Raciborz Day (imgw) [oC]', 'Average Temperature Raciborz Night (imgw) [oC]', 'Average Sensed Temperature Katowice Day [oC]', 'Average Sensed Temperature Katowice Night [oC]', 'Average Sensed Temperature Raciborz Day [oC]', 'Average Sensed Temperature Raciborz Night [oC]', 'Difference temperature Day Night Katowice (wttrin) [oC]', 'Difference Temperature Day Night Katowice (imgw) [oC]', 'Difference Temperature Day Night Raciborz (wttrin) [oC]', 'Difference Temperature Day Night Raciborz (imgw) [oC]', 'Difference Sensed Temperature Day Night Katowice [oC]', 'Difference Sensed Temperature Day Night Raciborz [oC]'}

output5 = [header; num2cell(M_average_T)]

diff_average_Katowice_wttrin = abs(average_T_Katowice_wttrin_Day - average_T_Katowice_wttrin_Night)
diff_average_Katowice_imgw = abs(average_T_Katowice_imgw_Day - average_T_Katowice_imgw_Night)

diff_average_Raciborz_wttrin = abs(average_T_Raciborz_wttrin_Day - average_T_Raciborz_wttrin_Night) 
diff_average_Raciborz_imgw = abs(average_T_Raciborz_imgw_Day - average_T_Raciborz_imgw_Night)

diff_average_sensed_Katowice = abs(average_T_sensed_Katowice_Day - average_T_sensed_Katowice_Night)
diff_average_sensed_Raciborz = abs(average_T_sensed_Raciborz_Day - average_T_sensed_Raciborz_Night)

dlmwrite('weatherData.csv',M_average_T,'delimiter',',','-append');

%displaying maximal values of temperature
max_T_Katowice_wttrin_Day = max(T_Katowice_wttrin_Day{:, 6})
max_T_Katowice_wttrin_Night = max(T_Katowice_wttrin_Night{:, 6})
max_T_Katowice_imgw_Day = max(T_Katowice_imgw_Day{:, 6})
max_T_Katowice_imgw_Night = max(T_Katowice_imgw_Night{:, 6})

max_T_Raciborz_wttrin_Day = max(T_Raciborz_wttrin_Day{:, 6})
max_T_Raciborz_wttrin_Night = max(T_Raciborz_wttrin_Night{:, 6})
max_T_Raciborz_imgw_Day = max(T_Raciborz_imgw_Day{:, 6})
max_T_Raciborz_imgw_Night = max(T_Raciborz_imgw_Night{:, 6})



